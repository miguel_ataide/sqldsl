package org.xtext.example.ssql.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.ssql.services.SsqlGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSsqlParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'insert'", "'('", "')'", "'entity'", "':'", "';'", "'='"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int RULE_INT=6;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalSsqlParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSsqlParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSsqlParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSsql.g"; }


    	private SsqlGrammarAccess grammarAccess;

    	public void setGrammarAccess(SsqlGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleTables"
    // InternalSsql.g:53:1: entryRuleTables : ruleTables EOF ;
    public final void entryRuleTables() throws RecognitionException {
        try {
            // InternalSsql.g:54:1: ( ruleTables EOF )
            // InternalSsql.g:55:1: ruleTables EOF
            {
             before(grammarAccess.getTablesRule()); 
            pushFollow(FOLLOW_1);
            ruleTables();

            state._fsp--;

             after(grammarAccess.getTablesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTables"


    // $ANTLR start "ruleTables"
    // InternalSsql.g:62:1: ruleTables : ( ( rule__Tables__ElementsAssignment )* ) ;
    public final void ruleTables() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:66:2: ( ( ( rule__Tables__ElementsAssignment )* ) )
            // InternalSsql.g:67:2: ( ( rule__Tables__ElementsAssignment )* )
            {
            // InternalSsql.g:67:2: ( ( rule__Tables__ElementsAssignment )* )
            // InternalSsql.g:68:3: ( rule__Tables__ElementsAssignment )*
            {
             before(grammarAccess.getTablesAccess().getElementsAssignment()); 
            // InternalSsql.g:69:3: ( rule__Tables__ElementsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11||LA1_0==14) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalSsql.g:69:4: rule__Tables__ElementsAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Tables__ElementsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getTablesAccess().getElementsAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTables"


    // $ANTLR start "entryRuleInsert"
    // InternalSsql.g:78:1: entryRuleInsert : ruleInsert EOF ;
    public final void entryRuleInsert() throws RecognitionException {
        try {
            // InternalSsql.g:79:1: ( ruleInsert EOF )
            // InternalSsql.g:80:1: ruleInsert EOF
            {
             before(grammarAccess.getInsertRule()); 
            pushFollow(FOLLOW_1);
            ruleInsert();

            state._fsp--;

             after(grammarAccess.getInsertRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInsert"


    // $ANTLR start "ruleInsert"
    // InternalSsql.g:87:1: ruleInsert : ( ( rule__Insert__Group__0 ) ) ;
    public final void ruleInsert() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:91:2: ( ( ( rule__Insert__Group__0 ) ) )
            // InternalSsql.g:92:2: ( ( rule__Insert__Group__0 ) )
            {
            // InternalSsql.g:92:2: ( ( rule__Insert__Group__0 ) )
            // InternalSsql.g:93:3: ( rule__Insert__Group__0 )
            {
             before(grammarAccess.getInsertAccess().getGroup()); 
            // InternalSsql.g:94:3: ( rule__Insert__Group__0 )
            // InternalSsql.g:94:4: rule__Insert__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Insert__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInsertAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInsert"


    // $ANTLR start "entryRuleFields"
    // InternalSsql.g:103:1: entryRuleFields : ruleFields EOF ;
    public final void entryRuleFields() throws RecognitionException {
        try {
            // InternalSsql.g:104:1: ( ruleFields EOF )
            // InternalSsql.g:105:1: ruleFields EOF
            {
             before(grammarAccess.getFieldsRule()); 
            pushFollow(FOLLOW_1);
            ruleFields();

            state._fsp--;

             after(grammarAccess.getFieldsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFields"


    // $ANTLR start "ruleFields"
    // InternalSsql.g:112:1: ruleFields : ( ( rule__Fields__Alternatives ) ) ;
    public final void ruleFields() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:116:2: ( ( ( rule__Fields__Alternatives ) ) )
            // InternalSsql.g:117:2: ( ( rule__Fields__Alternatives ) )
            {
            // InternalSsql.g:117:2: ( ( rule__Fields__Alternatives ) )
            // InternalSsql.g:118:3: ( rule__Fields__Alternatives )
            {
             before(grammarAccess.getFieldsAccess().getAlternatives()); 
            // InternalSsql.g:119:3: ( rule__Fields__Alternatives )
            // InternalSsql.g:119:4: rule__Fields__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Fields__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFieldsAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFields"


    // $ANTLR start "entryRuleEntity"
    // InternalSsql.g:128:1: entryRuleEntity : ruleEntity EOF ;
    public final void entryRuleEntity() throws RecognitionException {
        try {
            // InternalSsql.g:129:1: ( ruleEntity EOF )
            // InternalSsql.g:130:1: ruleEntity EOF
            {
             before(grammarAccess.getEntityRule()); 
            pushFollow(FOLLOW_1);
            ruleEntity();

            state._fsp--;

             after(grammarAccess.getEntityRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEntity"


    // $ANTLR start "ruleEntity"
    // InternalSsql.g:137:1: ruleEntity : ( ( rule__Entity__Group__0 ) ) ;
    public final void ruleEntity() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:141:2: ( ( ( rule__Entity__Group__0 ) ) )
            // InternalSsql.g:142:2: ( ( rule__Entity__Group__0 ) )
            {
            // InternalSsql.g:142:2: ( ( rule__Entity__Group__0 ) )
            // InternalSsql.g:143:3: ( rule__Entity__Group__0 )
            {
             before(grammarAccess.getEntityAccess().getGroup()); 
            // InternalSsql.g:144:3: ( rule__Entity__Group__0 )
            // InternalSsql.g:144:4: rule__Entity__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Entity__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEntityAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEntity"


    // $ANTLR start "entryRuleFeature"
    // InternalSsql.g:153:1: entryRuleFeature : ruleFeature EOF ;
    public final void entryRuleFeature() throws RecognitionException {
        try {
            // InternalSsql.g:154:1: ( ruleFeature EOF )
            // InternalSsql.g:155:1: ruleFeature EOF
            {
             before(grammarAccess.getFeatureRule()); 
            pushFollow(FOLLOW_1);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getFeatureRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // InternalSsql.g:162:1: ruleFeature : ( ( rule__Feature__Group__0 ) ) ;
    public final void ruleFeature() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:166:2: ( ( ( rule__Feature__Group__0 ) ) )
            // InternalSsql.g:167:2: ( ( rule__Feature__Group__0 ) )
            {
            // InternalSsql.g:167:2: ( ( rule__Feature__Group__0 ) )
            // InternalSsql.g:168:3: ( rule__Feature__Group__0 )
            {
             before(grammarAccess.getFeatureAccess().getGroup()); 
            // InternalSsql.g:169:3: ( rule__Feature__Group__0 )
            // InternalSsql.g:169:4: rule__Feature__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Feature__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleRow"
    // InternalSsql.g:178:1: entryRuleRow : ruleRow EOF ;
    public final void entryRuleRow() throws RecognitionException {
        try {
            // InternalSsql.g:179:1: ( ruleRow EOF )
            // InternalSsql.g:180:1: ruleRow EOF
            {
             before(grammarAccess.getRowRule()); 
            pushFollow(FOLLOW_1);
            ruleRow();

            state._fsp--;

             after(grammarAccess.getRowRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRow"


    // $ANTLR start "ruleRow"
    // InternalSsql.g:187:1: ruleRow : ( ( rule__Row__Group__0 ) ) ;
    public final void ruleRow() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:191:2: ( ( ( rule__Row__Group__0 ) ) )
            // InternalSsql.g:192:2: ( ( rule__Row__Group__0 ) )
            {
            // InternalSsql.g:192:2: ( ( rule__Row__Group__0 ) )
            // InternalSsql.g:193:3: ( rule__Row__Group__0 )
            {
             before(grammarAccess.getRowAccess().getGroup()); 
            // InternalSsql.g:194:3: ( rule__Row__Group__0 )
            // InternalSsql.g:194:4: rule__Row__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Row__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRowAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRow"


    // $ANTLR start "entryRuleColumn"
    // InternalSsql.g:203:1: entryRuleColumn : ruleColumn EOF ;
    public final void entryRuleColumn() throws RecognitionException {
        try {
            // InternalSsql.g:204:1: ( ruleColumn EOF )
            // InternalSsql.g:205:1: ruleColumn EOF
            {
             before(grammarAccess.getColumnRule()); 
            pushFollow(FOLLOW_1);
            ruleColumn();

            state._fsp--;

             after(grammarAccess.getColumnRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleColumn"


    // $ANTLR start "ruleColumn"
    // InternalSsql.g:212:1: ruleColumn : ( ( rule__Column__Group__0 ) ) ;
    public final void ruleColumn() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:216:2: ( ( ( rule__Column__Group__0 ) ) )
            // InternalSsql.g:217:2: ( ( rule__Column__Group__0 ) )
            {
            // InternalSsql.g:217:2: ( ( rule__Column__Group__0 ) )
            // InternalSsql.g:218:3: ( rule__Column__Group__0 )
            {
             before(grammarAccess.getColumnAccess().getGroup()); 
            // InternalSsql.g:219:3: ( rule__Column__Group__0 )
            // InternalSsql.g:219:4: rule__Column__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Column__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getColumnAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleColumn"


    // $ANTLR start "rule__Fields__Alternatives"
    // InternalSsql.g:227:1: rule__Fields__Alternatives : ( ( ruleEntity ) | ( ruleInsert ) );
    public final void rule__Fields__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:231:1: ( ( ruleEntity ) | ( ruleInsert ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==14) ) {
                alt2=1;
            }
            else if ( (LA2_0==11) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalSsql.g:232:2: ( ruleEntity )
                    {
                    // InternalSsql.g:232:2: ( ruleEntity )
                    // InternalSsql.g:233:3: ruleEntity
                    {
                     before(grammarAccess.getFieldsAccess().getEntityParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleEntity();

                    state._fsp--;

                     after(grammarAccess.getFieldsAccess().getEntityParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalSsql.g:238:2: ( ruleInsert )
                    {
                    // InternalSsql.g:238:2: ( ruleInsert )
                    // InternalSsql.g:239:3: ruleInsert
                    {
                     before(grammarAccess.getFieldsAccess().getInsertParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleInsert();

                    state._fsp--;

                     after(grammarAccess.getFieldsAccess().getInsertParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fields__Alternatives"


    // $ANTLR start "rule__Insert__Group__0"
    // InternalSsql.g:248:1: rule__Insert__Group__0 : rule__Insert__Group__0__Impl rule__Insert__Group__1 ;
    public final void rule__Insert__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:252:1: ( rule__Insert__Group__0__Impl rule__Insert__Group__1 )
            // InternalSsql.g:253:2: rule__Insert__Group__0__Impl rule__Insert__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Insert__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Insert__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Insert__Group__0"


    // $ANTLR start "rule__Insert__Group__0__Impl"
    // InternalSsql.g:260:1: rule__Insert__Group__0__Impl : ( 'insert' ) ;
    public final void rule__Insert__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:264:1: ( ( 'insert' ) )
            // InternalSsql.g:265:1: ( 'insert' )
            {
            // InternalSsql.g:265:1: ( 'insert' )
            // InternalSsql.g:266:2: 'insert'
            {
             before(grammarAccess.getInsertAccess().getInsertKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getInsertAccess().getInsertKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Insert__Group__0__Impl"


    // $ANTLR start "rule__Insert__Group__1"
    // InternalSsql.g:275:1: rule__Insert__Group__1 : rule__Insert__Group__1__Impl rule__Insert__Group__2 ;
    public final void rule__Insert__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:279:1: ( rule__Insert__Group__1__Impl rule__Insert__Group__2 )
            // InternalSsql.g:280:2: rule__Insert__Group__1__Impl rule__Insert__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Insert__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Insert__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Insert__Group__1"


    // $ANTLR start "rule__Insert__Group__1__Impl"
    // InternalSsql.g:287:1: rule__Insert__Group__1__Impl : ( ( rule__Insert__TableAssignment_1 ) ) ;
    public final void rule__Insert__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:291:1: ( ( ( rule__Insert__TableAssignment_1 ) ) )
            // InternalSsql.g:292:1: ( ( rule__Insert__TableAssignment_1 ) )
            {
            // InternalSsql.g:292:1: ( ( rule__Insert__TableAssignment_1 ) )
            // InternalSsql.g:293:2: ( rule__Insert__TableAssignment_1 )
            {
             before(grammarAccess.getInsertAccess().getTableAssignment_1()); 
            // InternalSsql.g:294:2: ( rule__Insert__TableAssignment_1 )
            // InternalSsql.g:294:3: rule__Insert__TableAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Insert__TableAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getInsertAccess().getTableAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Insert__Group__1__Impl"


    // $ANTLR start "rule__Insert__Group__2"
    // InternalSsql.g:302:1: rule__Insert__Group__2 : rule__Insert__Group__2__Impl rule__Insert__Group__3 ;
    public final void rule__Insert__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:306:1: ( rule__Insert__Group__2__Impl rule__Insert__Group__3 )
            // InternalSsql.g:307:2: rule__Insert__Group__2__Impl rule__Insert__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__Insert__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Insert__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Insert__Group__2"


    // $ANTLR start "rule__Insert__Group__2__Impl"
    // InternalSsql.g:314:1: rule__Insert__Group__2__Impl : ( '(' ) ;
    public final void rule__Insert__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:318:1: ( ( '(' ) )
            // InternalSsql.g:319:1: ( '(' )
            {
            // InternalSsql.g:319:1: ( '(' )
            // InternalSsql.g:320:2: '('
            {
             before(grammarAccess.getInsertAccess().getLeftParenthesisKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getInsertAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Insert__Group__2__Impl"


    // $ANTLR start "rule__Insert__Group__3"
    // InternalSsql.g:329:1: rule__Insert__Group__3 : rule__Insert__Group__3__Impl rule__Insert__Group__4 ;
    public final void rule__Insert__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:333:1: ( rule__Insert__Group__3__Impl rule__Insert__Group__4 )
            // InternalSsql.g:334:2: rule__Insert__Group__3__Impl rule__Insert__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Insert__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Insert__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Insert__Group__3"


    // $ANTLR start "rule__Insert__Group__3__Impl"
    // InternalSsql.g:341:1: rule__Insert__Group__3__Impl : ( ( rule__Insert__RowsAssignment_3 )* ) ;
    public final void rule__Insert__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:345:1: ( ( ( rule__Insert__RowsAssignment_3 )* ) )
            // InternalSsql.g:346:1: ( ( rule__Insert__RowsAssignment_3 )* )
            {
            // InternalSsql.g:346:1: ( ( rule__Insert__RowsAssignment_3 )* )
            // InternalSsql.g:347:2: ( rule__Insert__RowsAssignment_3 )*
            {
             before(grammarAccess.getInsertAccess().getRowsAssignment_3()); 
            // InternalSsql.g:348:2: ( rule__Insert__RowsAssignment_3 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID||LA3_0==16) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalSsql.g:348:3: rule__Insert__RowsAssignment_3
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Insert__RowsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getInsertAccess().getRowsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Insert__Group__3__Impl"


    // $ANTLR start "rule__Insert__Group__4"
    // InternalSsql.g:356:1: rule__Insert__Group__4 : rule__Insert__Group__4__Impl ;
    public final void rule__Insert__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:360:1: ( rule__Insert__Group__4__Impl )
            // InternalSsql.g:361:2: rule__Insert__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Insert__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Insert__Group__4"


    // $ANTLR start "rule__Insert__Group__4__Impl"
    // InternalSsql.g:367:1: rule__Insert__Group__4__Impl : ( ')' ) ;
    public final void rule__Insert__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:371:1: ( ( ')' ) )
            // InternalSsql.g:372:1: ( ')' )
            {
            // InternalSsql.g:372:1: ( ')' )
            // InternalSsql.g:373:2: ')'
            {
             before(grammarAccess.getInsertAccess().getRightParenthesisKeyword_4()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getInsertAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Insert__Group__4__Impl"


    // $ANTLR start "rule__Entity__Group__0"
    // InternalSsql.g:383:1: rule__Entity__Group__0 : rule__Entity__Group__0__Impl rule__Entity__Group__1 ;
    public final void rule__Entity__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:387:1: ( rule__Entity__Group__0__Impl rule__Entity__Group__1 )
            // InternalSsql.g:388:2: rule__Entity__Group__0__Impl rule__Entity__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Entity__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__0"


    // $ANTLR start "rule__Entity__Group__0__Impl"
    // InternalSsql.g:395:1: rule__Entity__Group__0__Impl : ( 'entity' ) ;
    public final void rule__Entity__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:399:1: ( ( 'entity' ) )
            // InternalSsql.g:400:1: ( 'entity' )
            {
            // InternalSsql.g:400:1: ( 'entity' )
            // InternalSsql.g:401:2: 'entity'
            {
             before(grammarAccess.getEntityAccess().getEntityKeyword_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getEntityKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__0__Impl"


    // $ANTLR start "rule__Entity__Group__1"
    // InternalSsql.g:410:1: rule__Entity__Group__1 : rule__Entity__Group__1__Impl rule__Entity__Group__2 ;
    public final void rule__Entity__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:414:1: ( rule__Entity__Group__1__Impl rule__Entity__Group__2 )
            // InternalSsql.g:415:2: rule__Entity__Group__1__Impl rule__Entity__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__Entity__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__1"


    // $ANTLR start "rule__Entity__Group__1__Impl"
    // InternalSsql.g:422:1: rule__Entity__Group__1__Impl : ( ( rule__Entity__NameAssignment_1 ) ) ;
    public final void rule__Entity__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:426:1: ( ( ( rule__Entity__NameAssignment_1 ) ) )
            // InternalSsql.g:427:1: ( ( rule__Entity__NameAssignment_1 ) )
            {
            // InternalSsql.g:427:1: ( ( rule__Entity__NameAssignment_1 ) )
            // InternalSsql.g:428:2: ( rule__Entity__NameAssignment_1 )
            {
             before(grammarAccess.getEntityAccess().getNameAssignment_1()); 
            // InternalSsql.g:429:2: ( rule__Entity__NameAssignment_1 )
            // InternalSsql.g:429:3: rule__Entity__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Entity__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEntityAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__1__Impl"


    // $ANTLR start "rule__Entity__Group__2"
    // InternalSsql.g:437:1: rule__Entity__Group__2 : rule__Entity__Group__2__Impl rule__Entity__Group__3 ;
    public final void rule__Entity__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:441:1: ( rule__Entity__Group__2__Impl rule__Entity__Group__3 )
            // InternalSsql.g:442:2: rule__Entity__Group__2__Impl rule__Entity__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Entity__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__2"


    // $ANTLR start "rule__Entity__Group__2__Impl"
    // InternalSsql.g:449:1: rule__Entity__Group__2__Impl : ( '(' ) ;
    public final void rule__Entity__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:453:1: ( ( '(' ) )
            // InternalSsql.g:454:1: ( '(' )
            {
            // InternalSsql.g:454:1: ( '(' )
            // InternalSsql.g:455:2: '('
            {
             before(grammarAccess.getEntityAccess().getLeftParenthesisKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__2__Impl"


    // $ANTLR start "rule__Entity__Group__3"
    // InternalSsql.g:464:1: rule__Entity__Group__3 : rule__Entity__Group__3__Impl rule__Entity__Group__4 ;
    public final void rule__Entity__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:468:1: ( rule__Entity__Group__3__Impl rule__Entity__Group__4 )
            // InternalSsql.g:469:2: rule__Entity__Group__3__Impl rule__Entity__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__Entity__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Entity__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__3"


    // $ANTLR start "rule__Entity__Group__3__Impl"
    // InternalSsql.g:476:1: rule__Entity__Group__3__Impl : ( ( rule__Entity__FeaturesAssignment_3 )* ) ;
    public final void rule__Entity__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:480:1: ( ( ( rule__Entity__FeaturesAssignment_3 )* ) )
            // InternalSsql.g:481:1: ( ( rule__Entity__FeaturesAssignment_3 )* )
            {
            // InternalSsql.g:481:1: ( ( rule__Entity__FeaturesAssignment_3 )* )
            // InternalSsql.g:482:2: ( rule__Entity__FeaturesAssignment_3 )*
            {
             before(grammarAccess.getEntityAccess().getFeaturesAssignment_3()); 
            // InternalSsql.g:483:2: ( rule__Entity__FeaturesAssignment_3 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalSsql.g:483:3: rule__Entity__FeaturesAssignment_3
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Entity__FeaturesAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getEntityAccess().getFeaturesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__3__Impl"


    // $ANTLR start "rule__Entity__Group__4"
    // InternalSsql.g:491:1: rule__Entity__Group__4 : rule__Entity__Group__4__Impl ;
    public final void rule__Entity__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:495:1: ( rule__Entity__Group__4__Impl )
            // InternalSsql.g:496:2: rule__Entity__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Entity__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__4"


    // $ANTLR start "rule__Entity__Group__4__Impl"
    // InternalSsql.g:502:1: rule__Entity__Group__4__Impl : ( ')' ) ;
    public final void rule__Entity__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:506:1: ( ( ')' ) )
            // InternalSsql.g:507:1: ( ')' )
            {
            // InternalSsql.g:507:1: ( ')' )
            // InternalSsql.g:508:2: ')'
            {
             before(grammarAccess.getEntityAccess().getRightParenthesisKeyword_4()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__Group__4__Impl"


    // $ANTLR start "rule__Feature__Group__0"
    // InternalSsql.g:518:1: rule__Feature__Group__0 : rule__Feature__Group__0__Impl rule__Feature__Group__1 ;
    public final void rule__Feature__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:522:1: ( rule__Feature__Group__0__Impl rule__Feature__Group__1 )
            // InternalSsql.g:523:2: rule__Feature__Group__0__Impl rule__Feature__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Feature__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Feature__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__0"


    // $ANTLR start "rule__Feature__Group__0__Impl"
    // InternalSsql.g:530:1: rule__Feature__Group__0__Impl : ( ( rule__Feature__NameAssignment_0 ) ) ;
    public final void rule__Feature__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:534:1: ( ( ( rule__Feature__NameAssignment_0 ) ) )
            // InternalSsql.g:535:1: ( ( rule__Feature__NameAssignment_0 ) )
            {
            // InternalSsql.g:535:1: ( ( rule__Feature__NameAssignment_0 ) )
            // InternalSsql.g:536:2: ( rule__Feature__NameAssignment_0 )
            {
             before(grammarAccess.getFeatureAccess().getNameAssignment_0()); 
            // InternalSsql.g:537:2: ( rule__Feature__NameAssignment_0 )
            // InternalSsql.g:537:3: rule__Feature__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Feature__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__0__Impl"


    // $ANTLR start "rule__Feature__Group__1"
    // InternalSsql.g:545:1: rule__Feature__Group__1 : rule__Feature__Group__1__Impl rule__Feature__Group__2 ;
    public final void rule__Feature__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:549:1: ( rule__Feature__Group__1__Impl rule__Feature__Group__2 )
            // InternalSsql.g:550:2: rule__Feature__Group__1__Impl rule__Feature__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Feature__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Feature__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__1"


    // $ANTLR start "rule__Feature__Group__1__Impl"
    // InternalSsql.g:557:1: rule__Feature__Group__1__Impl : ( ':' ) ;
    public final void rule__Feature__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:561:1: ( ( ':' ) )
            // InternalSsql.g:562:1: ( ':' )
            {
            // InternalSsql.g:562:1: ( ':' )
            // InternalSsql.g:563:2: ':'
            {
             before(grammarAccess.getFeatureAccess().getColonKeyword_1()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getFeatureAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__1__Impl"


    // $ANTLR start "rule__Feature__Group__2"
    // InternalSsql.g:572:1: rule__Feature__Group__2 : rule__Feature__Group__2__Impl ;
    public final void rule__Feature__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:576:1: ( rule__Feature__Group__2__Impl )
            // InternalSsql.g:577:2: rule__Feature__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Feature__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__2"


    // $ANTLR start "rule__Feature__Group__2__Impl"
    // InternalSsql.g:583:1: rule__Feature__Group__2__Impl : ( ( rule__Feature__TypeAssignment_2 ) ) ;
    public final void rule__Feature__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:587:1: ( ( ( rule__Feature__TypeAssignment_2 ) ) )
            // InternalSsql.g:588:1: ( ( rule__Feature__TypeAssignment_2 ) )
            {
            // InternalSsql.g:588:1: ( ( rule__Feature__TypeAssignment_2 ) )
            // InternalSsql.g:589:2: ( rule__Feature__TypeAssignment_2 )
            {
             before(grammarAccess.getFeatureAccess().getTypeAssignment_2()); 
            // InternalSsql.g:590:2: ( rule__Feature__TypeAssignment_2 )
            // InternalSsql.g:590:3: rule__Feature__TypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Feature__TypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__2__Impl"


    // $ANTLR start "rule__Row__Group__0"
    // InternalSsql.g:599:1: rule__Row__Group__0 : rule__Row__Group__0__Impl rule__Row__Group__1 ;
    public final void rule__Row__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:603:1: ( rule__Row__Group__0__Impl rule__Row__Group__1 )
            // InternalSsql.g:604:2: rule__Row__Group__0__Impl rule__Row__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__Row__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Row__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__0"


    // $ANTLR start "rule__Row__Group__0__Impl"
    // InternalSsql.g:611:1: rule__Row__Group__0__Impl : ( ( rule__Row__ColumnsAssignment_0 )* ) ;
    public final void rule__Row__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:615:1: ( ( ( rule__Row__ColumnsAssignment_0 )* ) )
            // InternalSsql.g:616:1: ( ( rule__Row__ColumnsAssignment_0 )* )
            {
            // InternalSsql.g:616:1: ( ( rule__Row__ColumnsAssignment_0 )* )
            // InternalSsql.g:617:2: ( rule__Row__ColumnsAssignment_0 )*
            {
             before(grammarAccess.getRowAccess().getColumnsAssignment_0()); 
            // InternalSsql.g:618:2: ( rule__Row__ColumnsAssignment_0 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalSsql.g:618:3: rule__Row__ColumnsAssignment_0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Row__ColumnsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getRowAccess().getColumnsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__0__Impl"


    // $ANTLR start "rule__Row__Group__1"
    // InternalSsql.g:626:1: rule__Row__Group__1 : rule__Row__Group__1__Impl ;
    public final void rule__Row__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:630:1: ( rule__Row__Group__1__Impl )
            // InternalSsql.g:631:2: rule__Row__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Row__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__1"


    // $ANTLR start "rule__Row__Group__1__Impl"
    // InternalSsql.g:637:1: rule__Row__Group__1__Impl : ( ';' ) ;
    public final void rule__Row__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:641:1: ( ( ';' ) )
            // InternalSsql.g:642:1: ( ';' )
            {
            // InternalSsql.g:642:1: ( ';' )
            // InternalSsql.g:643:2: ';'
            {
             before(grammarAccess.getRowAccess().getSemicolonKeyword_1()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getRowAccess().getSemicolonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__Group__1__Impl"


    // $ANTLR start "rule__Column__Group__0"
    // InternalSsql.g:653:1: rule__Column__Group__0 : rule__Column__Group__0__Impl rule__Column__Group__1 ;
    public final void rule__Column__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:657:1: ( rule__Column__Group__0__Impl rule__Column__Group__1 )
            // InternalSsql.g:658:2: rule__Column__Group__0__Impl rule__Column__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__Column__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Column__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Column__Group__0"


    // $ANTLR start "rule__Column__Group__0__Impl"
    // InternalSsql.g:665:1: rule__Column__Group__0__Impl : ( ( rule__Column__NameAssignment_0 ) ) ;
    public final void rule__Column__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:669:1: ( ( ( rule__Column__NameAssignment_0 ) ) )
            // InternalSsql.g:670:1: ( ( rule__Column__NameAssignment_0 ) )
            {
            // InternalSsql.g:670:1: ( ( rule__Column__NameAssignment_0 ) )
            // InternalSsql.g:671:2: ( rule__Column__NameAssignment_0 )
            {
             before(grammarAccess.getColumnAccess().getNameAssignment_0()); 
            // InternalSsql.g:672:2: ( rule__Column__NameAssignment_0 )
            // InternalSsql.g:672:3: rule__Column__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Column__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getColumnAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Column__Group__0__Impl"


    // $ANTLR start "rule__Column__Group__1"
    // InternalSsql.g:680:1: rule__Column__Group__1 : rule__Column__Group__1__Impl rule__Column__Group__2 ;
    public final void rule__Column__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:684:1: ( rule__Column__Group__1__Impl rule__Column__Group__2 )
            // InternalSsql.g:685:2: rule__Column__Group__1__Impl rule__Column__Group__2
            {
            pushFollow(FOLLOW_13);
            rule__Column__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Column__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Column__Group__1"


    // $ANTLR start "rule__Column__Group__1__Impl"
    // InternalSsql.g:692:1: rule__Column__Group__1__Impl : ( '=' ) ;
    public final void rule__Column__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:696:1: ( ( '=' ) )
            // InternalSsql.g:697:1: ( '=' )
            {
            // InternalSsql.g:697:1: ( '=' )
            // InternalSsql.g:698:2: '='
            {
             before(grammarAccess.getColumnAccess().getEqualsSignKeyword_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getColumnAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Column__Group__1__Impl"


    // $ANTLR start "rule__Column__Group__2"
    // InternalSsql.g:707:1: rule__Column__Group__2 : rule__Column__Group__2__Impl ;
    public final void rule__Column__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:711:1: ( rule__Column__Group__2__Impl )
            // InternalSsql.g:712:2: rule__Column__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Column__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Column__Group__2"


    // $ANTLR start "rule__Column__Group__2__Impl"
    // InternalSsql.g:718:1: rule__Column__Group__2__Impl : ( ( rule__Column__ValueAssignment_2 ) ) ;
    public final void rule__Column__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:722:1: ( ( ( rule__Column__ValueAssignment_2 ) ) )
            // InternalSsql.g:723:1: ( ( rule__Column__ValueAssignment_2 ) )
            {
            // InternalSsql.g:723:1: ( ( rule__Column__ValueAssignment_2 ) )
            // InternalSsql.g:724:2: ( rule__Column__ValueAssignment_2 )
            {
             before(grammarAccess.getColumnAccess().getValueAssignment_2()); 
            // InternalSsql.g:725:2: ( rule__Column__ValueAssignment_2 )
            // InternalSsql.g:725:3: rule__Column__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Column__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getColumnAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Column__Group__2__Impl"


    // $ANTLR start "rule__Tables__ElementsAssignment"
    // InternalSsql.g:734:1: rule__Tables__ElementsAssignment : ( ruleFields ) ;
    public final void rule__Tables__ElementsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:738:1: ( ( ruleFields ) )
            // InternalSsql.g:739:2: ( ruleFields )
            {
            // InternalSsql.g:739:2: ( ruleFields )
            // InternalSsql.g:740:3: ruleFields
            {
             before(grammarAccess.getTablesAccess().getElementsFieldsParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleFields();

            state._fsp--;

             after(grammarAccess.getTablesAccess().getElementsFieldsParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Tables__ElementsAssignment"


    // $ANTLR start "rule__Insert__TableAssignment_1"
    // InternalSsql.g:749:1: rule__Insert__TableAssignment_1 : ( RULE_ID ) ;
    public final void rule__Insert__TableAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:753:1: ( ( RULE_ID ) )
            // InternalSsql.g:754:2: ( RULE_ID )
            {
            // InternalSsql.g:754:2: ( RULE_ID )
            // InternalSsql.g:755:3: RULE_ID
            {
             before(grammarAccess.getInsertAccess().getTableIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getInsertAccess().getTableIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Insert__TableAssignment_1"


    // $ANTLR start "rule__Insert__RowsAssignment_3"
    // InternalSsql.g:764:1: rule__Insert__RowsAssignment_3 : ( ruleRow ) ;
    public final void rule__Insert__RowsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:768:1: ( ( ruleRow ) )
            // InternalSsql.g:769:2: ( ruleRow )
            {
            // InternalSsql.g:769:2: ( ruleRow )
            // InternalSsql.g:770:3: ruleRow
            {
             before(grammarAccess.getInsertAccess().getRowsRowParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleRow();

            state._fsp--;

             after(grammarAccess.getInsertAccess().getRowsRowParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Insert__RowsAssignment_3"


    // $ANTLR start "rule__Entity__NameAssignment_1"
    // InternalSsql.g:779:1: rule__Entity__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Entity__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:783:1: ( ( RULE_ID ) )
            // InternalSsql.g:784:2: ( RULE_ID )
            {
            // InternalSsql.g:784:2: ( RULE_ID )
            // InternalSsql.g:785:3: RULE_ID
            {
             before(grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__NameAssignment_1"


    // $ANTLR start "rule__Entity__FeaturesAssignment_3"
    // InternalSsql.g:794:1: rule__Entity__FeaturesAssignment_3 : ( ruleFeature ) ;
    public final void rule__Entity__FeaturesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:798:1: ( ( ruleFeature ) )
            // InternalSsql.g:799:2: ( ruleFeature )
            {
            // InternalSsql.g:799:2: ( ruleFeature )
            // InternalSsql.g:800:3: ruleFeature
            {
             before(grammarAccess.getEntityAccess().getFeaturesFeatureParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getEntityAccess().getFeaturesFeatureParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Entity__FeaturesAssignment_3"


    // $ANTLR start "rule__Feature__NameAssignment_0"
    // InternalSsql.g:809:1: rule__Feature__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Feature__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:813:1: ( ( RULE_ID ) )
            // InternalSsql.g:814:2: ( RULE_ID )
            {
            // InternalSsql.g:814:2: ( RULE_ID )
            // InternalSsql.g:815:3: RULE_ID
            {
             before(grammarAccess.getFeatureAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFeatureAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__NameAssignment_0"


    // $ANTLR start "rule__Feature__TypeAssignment_2"
    // InternalSsql.g:824:1: rule__Feature__TypeAssignment_2 : ( RULE_ID ) ;
    public final void rule__Feature__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:828:1: ( ( RULE_ID ) )
            // InternalSsql.g:829:2: ( RULE_ID )
            {
            // InternalSsql.g:829:2: ( RULE_ID )
            // InternalSsql.g:830:3: RULE_ID
            {
             before(grammarAccess.getFeatureAccess().getTypeIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFeatureAccess().getTypeIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__TypeAssignment_2"


    // $ANTLR start "rule__Row__ColumnsAssignment_0"
    // InternalSsql.g:839:1: rule__Row__ColumnsAssignment_0 : ( ruleColumn ) ;
    public final void rule__Row__ColumnsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:843:1: ( ( ruleColumn ) )
            // InternalSsql.g:844:2: ( ruleColumn )
            {
            // InternalSsql.g:844:2: ( ruleColumn )
            // InternalSsql.g:845:3: ruleColumn
            {
             before(grammarAccess.getRowAccess().getColumnsColumnParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleColumn();

            state._fsp--;

             after(grammarAccess.getRowAccess().getColumnsColumnParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Row__ColumnsAssignment_0"


    // $ANTLR start "rule__Column__NameAssignment_0"
    // InternalSsql.g:854:1: rule__Column__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Column__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:858:1: ( ( RULE_ID ) )
            // InternalSsql.g:859:2: ( RULE_ID )
            {
            // InternalSsql.g:859:2: ( RULE_ID )
            // InternalSsql.g:860:3: RULE_ID
            {
             before(grammarAccess.getColumnAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getColumnAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Column__NameAssignment_0"


    // $ANTLR start "rule__Column__ValueAssignment_2"
    // InternalSsql.g:869:1: rule__Column__ValueAssignment_2 : ( RULE_STRING ) ;
    public final void rule__Column__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalSsql.g:873:1: ( ( RULE_STRING ) )
            // InternalSsql.g:874:2: ( RULE_STRING )
            {
            // InternalSsql.g:874:2: ( RULE_STRING )
            // InternalSsql.g:875:3: RULE_STRING
            {
             before(grammarAccess.getColumnAccess().getValueSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getColumnAccess().getValueSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Column__ValueAssignment_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000004802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000012010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000010012L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000002010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000010010L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000020L});

}