package org.xtext.example.ssql.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.ssql.services.SsqlGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSsqlParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'insert'", "'('", "')'", "'entity'", "':'", "';'", "'='"
    };
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_STRING=5;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int RULE_INT=6;
    public static final int T__11=11;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalSsqlParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSsqlParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSsqlParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSsql.g"; }



     	private SsqlGrammarAccess grammarAccess;

        public InternalSsqlParser(TokenStream input, SsqlGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Tables";
       	}

       	@Override
       	protected SsqlGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleTables"
    // InternalSsql.g:64:1: entryRuleTables returns [EObject current=null] : iv_ruleTables= ruleTables EOF ;
    public final EObject entryRuleTables() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTables = null;


        try {
            // InternalSsql.g:64:47: (iv_ruleTables= ruleTables EOF )
            // InternalSsql.g:65:2: iv_ruleTables= ruleTables EOF
            {
             newCompositeNode(grammarAccess.getTablesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTables=ruleTables();

            state._fsp--;

             current =iv_ruleTables; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTables"


    // $ANTLR start "ruleTables"
    // InternalSsql.g:71:1: ruleTables returns [EObject current=null] : ( (lv_elements_0_0= ruleFields ) )* ;
    public final EObject ruleTables() throws RecognitionException {
        EObject current = null;

        EObject lv_elements_0_0 = null;



        	enterRule();

        try {
            // InternalSsql.g:77:2: ( ( (lv_elements_0_0= ruleFields ) )* )
            // InternalSsql.g:78:2: ( (lv_elements_0_0= ruleFields ) )*
            {
            // InternalSsql.g:78:2: ( (lv_elements_0_0= ruleFields ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11||LA1_0==14) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalSsql.g:79:3: (lv_elements_0_0= ruleFields )
            	    {
            	    // InternalSsql.g:79:3: (lv_elements_0_0= ruleFields )
            	    // InternalSsql.g:80:4: lv_elements_0_0= ruleFields
            	    {

            	    				newCompositeNode(grammarAccess.getTablesAccess().getElementsFieldsParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_elements_0_0=ruleFields();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getTablesRule());
            	    				}
            	    				add(
            	    					current,
            	    					"elements",
            	    					lv_elements_0_0,
            	    					"org.xtext.example.ssql.Ssql.Fields");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTables"


    // $ANTLR start "entryRuleInsert"
    // InternalSsql.g:100:1: entryRuleInsert returns [EObject current=null] : iv_ruleInsert= ruleInsert EOF ;
    public final EObject entryRuleInsert() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInsert = null;


        try {
            // InternalSsql.g:100:47: (iv_ruleInsert= ruleInsert EOF )
            // InternalSsql.g:101:2: iv_ruleInsert= ruleInsert EOF
            {
             newCompositeNode(grammarAccess.getInsertRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInsert=ruleInsert();

            state._fsp--;

             current =iv_ruleInsert; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInsert"


    // $ANTLR start "ruleInsert"
    // InternalSsql.g:107:1: ruleInsert returns [EObject current=null] : (otherlv_0= 'insert' ( (lv_table_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_rows_3_0= ruleRow ) )* otherlv_4= ')' ) ;
    public final EObject ruleInsert() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_table_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_rows_3_0 = null;



        	enterRule();

        try {
            // InternalSsql.g:113:2: ( (otherlv_0= 'insert' ( (lv_table_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_rows_3_0= ruleRow ) )* otherlv_4= ')' ) )
            // InternalSsql.g:114:2: (otherlv_0= 'insert' ( (lv_table_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_rows_3_0= ruleRow ) )* otherlv_4= ')' )
            {
            // InternalSsql.g:114:2: (otherlv_0= 'insert' ( (lv_table_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_rows_3_0= ruleRow ) )* otherlv_4= ')' )
            // InternalSsql.g:115:3: otherlv_0= 'insert' ( (lv_table_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_rows_3_0= ruleRow ) )* otherlv_4= ')'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getInsertAccess().getInsertKeyword_0());
            		
            // InternalSsql.g:119:3: ( (lv_table_1_0= RULE_ID ) )
            // InternalSsql.g:120:4: (lv_table_1_0= RULE_ID )
            {
            // InternalSsql.g:120:4: (lv_table_1_0= RULE_ID )
            // InternalSsql.g:121:5: lv_table_1_0= RULE_ID
            {
            lv_table_1_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_table_1_0, grammarAccess.getInsertAccess().getTableIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getInsertRule());
            					}
            					setWithLastConsumed(
            						current,
            						"table",
            						lv_table_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_2, grammarAccess.getInsertAccess().getLeftParenthesisKeyword_2());
            		
            // InternalSsql.g:141:3: ( (lv_rows_3_0= ruleRow ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID||LA2_0==16) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalSsql.g:142:4: (lv_rows_3_0= ruleRow )
            	    {
            	    // InternalSsql.g:142:4: (lv_rows_3_0= ruleRow )
            	    // InternalSsql.g:143:5: lv_rows_3_0= ruleRow
            	    {

            	    					newCompositeNode(grammarAccess.getInsertAccess().getRowsRowParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_rows_3_0=ruleRow();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getInsertRule());
            	    					}
            	    					add(
            	    						current,
            	    						"rows",
            	    						lv_rows_3_0,
            	    						"org.xtext.example.ssql.Ssql.Row");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_4=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getInsertAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInsert"


    // $ANTLR start "entryRuleFields"
    // InternalSsql.g:168:1: entryRuleFields returns [EObject current=null] : iv_ruleFields= ruleFields EOF ;
    public final EObject entryRuleFields() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFields = null;


        try {
            // InternalSsql.g:168:47: (iv_ruleFields= ruleFields EOF )
            // InternalSsql.g:169:2: iv_ruleFields= ruleFields EOF
            {
             newCompositeNode(grammarAccess.getFieldsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFields=ruleFields();

            state._fsp--;

             current =iv_ruleFields; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFields"


    // $ANTLR start "ruleFields"
    // InternalSsql.g:175:1: ruleFields returns [EObject current=null] : (this_Entity_0= ruleEntity | this_Insert_1= ruleInsert ) ;
    public final EObject ruleFields() throws RecognitionException {
        EObject current = null;

        EObject this_Entity_0 = null;

        EObject this_Insert_1 = null;



        	enterRule();

        try {
            // InternalSsql.g:181:2: ( (this_Entity_0= ruleEntity | this_Insert_1= ruleInsert ) )
            // InternalSsql.g:182:2: (this_Entity_0= ruleEntity | this_Insert_1= ruleInsert )
            {
            // InternalSsql.g:182:2: (this_Entity_0= ruleEntity | this_Insert_1= ruleInsert )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==14) ) {
                alt3=1;
            }
            else if ( (LA3_0==11) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalSsql.g:183:3: this_Entity_0= ruleEntity
                    {

                    			newCompositeNode(grammarAccess.getFieldsAccess().getEntityParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Entity_0=ruleEntity();

                    state._fsp--;


                    			current = this_Entity_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalSsql.g:192:3: this_Insert_1= ruleInsert
                    {

                    			newCompositeNode(grammarAccess.getFieldsAccess().getInsertParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Insert_1=ruleInsert();

                    state._fsp--;


                    			current = this_Insert_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFields"


    // $ANTLR start "entryRuleEntity"
    // InternalSsql.g:204:1: entryRuleEntity returns [EObject current=null] : iv_ruleEntity= ruleEntity EOF ;
    public final EObject entryRuleEntity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEntity = null;


        try {
            // InternalSsql.g:204:47: (iv_ruleEntity= ruleEntity EOF )
            // InternalSsql.g:205:2: iv_ruleEntity= ruleEntity EOF
            {
             newCompositeNode(grammarAccess.getEntityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEntity=ruleEntity();

            state._fsp--;

             current =iv_ruleEntity; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEntity"


    // $ANTLR start "ruleEntity"
    // InternalSsql.g:211:1: ruleEntity returns [EObject current=null] : (otherlv_0= 'entity' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_features_3_0= ruleFeature ) )* otherlv_4= ')' ) ;
    public final EObject ruleEntity() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_features_3_0 = null;



        	enterRule();

        try {
            // InternalSsql.g:217:2: ( (otherlv_0= 'entity' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_features_3_0= ruleFeature ) )* otherlv_4= ')' ) )
            // InternalSsql.g:218:2: (otherlv_0= 'entity' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_features_3_0= ruleFeature ) )* otherlv_4= ')' )
            {
            // InternalSsql.g:218:2: (otherlv_0= 'entity' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_features_3_0= ruleFeature ) )* otherlv_4= ')' )
            // InternalSsql.g:219:3: otherlv_0= 'entity' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_features_3_0= ruleFeature ) )* otherlv_4= ')'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getEntityAccess().getEntityKeyword_0());
            		
            // InternalSsql.g:223:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalSsql.g:224:4: (lv_name_1_0= RULE_ID )
            {
            // InternalSsql.g:224:4: (lv_name_1_0= RULE_ID )
            // InternalSsql.g:225:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_1_0, grammarAccess.getEntityAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEntityRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_7); 

            			newLeafNode(otherlv_2, grammarAccess.getEntityAccess().getLeftParenthesisKeyword_2());
            		
            // InternalSsql.g:245:3: ( (lv_features_3_0= ruleFeature ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalSsql.g:246:4: (lv_features_3_0= ruleFeature )
            	    {
            	    // InternalSsql.g:246:4: (lv_features_3_0= ruleFeature )
            	    // InternalSsql.g:247:5: lv_features_3_0= ruleFeature
            	    {

            	    					newCompositeNode(grammarAccess.getEntityAccess().getFeaturesFeatureParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_features_3_0=ruleFeature();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEntityRule());
            	    					}
            	    					add(
            	    						current,
            	    						"features",
            	    						lv_features_3_0,
            	    						"org.xtext.example.ssql.Ssql.Feature");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_4=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getEntityAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEntity"


    // $ANTLR start "entryRuleFeature"
    // InternalSsql.g:272:1: entryRuleFeature returns [EObject current=null] : iv_ruleFeature= ruleFeature EOF ;
    public final EObject entryRuleFeature() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeature = null;


        try {
            // InternalSsql.g:272:48: (iv_ruleFeature= ruleFeature EOF )
            // InternalSsql.g:273:2: iv_ruleFeature= ruleFeature EOF
            {
             newCompositeNode(grammarAccess.getFeatureRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeature=ruleFeature();

            state._fsp--;

             current =iv_ruleFeature; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // InternalSsql.g:279:1: ruleFeature returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= RULE_ID ) ) ) ;
    public final EObject ruleFeature() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_type_2_0=null;


        	enterRule();

        try {
            // InternalSsql.g:285:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= RULE_ID ) ) ) )
            // InternalSsql.g:286:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= RULE_ID ) ) )
            {
            // InternalSsql.g:286:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= RULE_ID ) ) )
            // InternalSsql.g:287:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= RULE_ID ) )
            {
            // InternalSsql.g:287:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalSsql.g:288:4: (lv_name_0_0= RULE_ID )
            {
            // InternalSsql.g:288:4: (lv_name_0_0= RULE_ID )
            // InternalSsql.g:289:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(lv_name_0_0, grammarAccess.getFeatureAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFeatureRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,15,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getFeatureAccess().getColonKeyword_1());
            		
            // InternalSsql.g:309:3: ( (lv_type_2_0= RULE_ID ) )
            // InternalSsql.g:310:4: (lv_type_2_0= RULE_ID )
            {
            // InternalSsql.g:310:4: (lv_type_2_0= RULE_ID )
            // InternalSsql.g:311:5: lv_type_2_0= RULE_ID
            {
            lv_type_2_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_type_2_0, grammarAccess.getFeatureAccess().getTypeIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFeatureRule());
            					}
            					setWithLastConsumed(
            						current,
            						"type",
            						lv_type_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleRow"
    // InternalSsql.g:331:1: entryRuleRow returns [EObject current=null] : iv_ruleRow= ruleRow EOF ;
    public final EObject entryRuleRow() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRow = null;


        try {
            // InternalSsql.g:331:44: (iv_ruleRow= ruleRow EOF )
            // InternalSsql.g:332:2: iv_ruleRow= ruleRow EOF
            {
             newCompositeNode(grammarAccess.getRowRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRow=ruleRow();

            state._fsp--;

             current =iv_ruleRow; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRow"


    // $ANTLR start "ruleRow"
    // InternalSsql.g:338:1: ruleRow returns [EObject current=null] : ( ( (lv_columns_0_0= ruleColumn ) )* otherlv_1= ';' ) ;
    public final EObject ruleRow() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_columns_0_0 = null;



        	enterRule();

        try {
            // InternalSsql.g:344:2: ( ( ( (lv_columns_0_0= ruleColumn ) )* otherlv_1= ';' ) )
            // InternalSsql.g:345:2: ( ( (lv_columns_0_0= ruleColumn ) )* otherlv_1= ';' )
            {
            // InternalSsql.g:345:2: ( ( (lv_columns_0_0= ruleColumn ) )* otherlv_1= ';' )
            // InternalSsql.g:346:3: ( (lv_columns_0_0= ruleColumn ) )* otherlv_1= ';'
            {
            // InternalSsql.g:346:3: ( (lv_columns_0_0= ruleColumn ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalSsql.g:347:4: (lv_columns_0_0= ruleColumn )
            	    {
            	    // InternalSsql.g:347:4: (lv_columns_0_0= ruleColumn )
            	    // InternalSsql.g:348:5: lv_columns_0_0= ruleColumn
            	    {

            	    					newCompositeNode(grammarAccess.getRowAccess().getColumnsColumnParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_9);
            	    lv_columns_0_0=ruleColumn();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getRowRule());
            	    					}
            	    					add(
            	    						current,
            	    						"columns",
            	    						lv_columns_0_0,
            	    						"org.xtext.example.ssql.Ssql.Column");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_1=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getRowAccess().getSemicolonKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRow"


    // $ANTLR start "entryRuleColumn"
    // InternalSsql.g:373:1: entryRuleColumn returns [EObject current=null] : iv_ruleColumn= ruleColumn EOF ;
    public final EObject entryRuleColumn() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleColumn = null;


        try {
            // InternalSsql.g:373:47: (iv_ruleColumn= ruleColumn EOF )
            // InternalSsql.g:374:2: iv_ruleColumn= ruleColumn EOF
            {
             newCompositeNode(grammarAccess.getColumnRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleColumn=ruleColumn();

            state._fsp--;

             current =iv_ruleColumn; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleColumn"


    // $ANTLR start "ruleColumn"
    // InternalSsql.g:380:1: ruleColumn returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_STRING ) ) ) ;
    public final EObject ruleColumn() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_value_2_0=null;


        	enterRule();

        try {
            // InternalSsql.g:386:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_STRING ) ) ) )
            // InternalSsql.g:387:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_STRING ) ) )
            {
            // InternalSsql.g:387:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_STRING ) ) )
            // InternalSsql.g:388:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( (lv_value_2_0= RULE_STRING ) )
            {
            // InternalSsql.g:388:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalSsql.g:389:4: (lv_name_0_0= RULE_ID )
            {
            // InternalSsql.g:389:4: (lv_name_0_0= RULE_ID )
            // InternalSsql.g:390:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_10); 

            					newLeafNode(lv_name_0_0, grammarAccess.getColumnAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getColumnRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,17,FOLLOW_11); 

            			newLeafNode(otherlv_1, grammarAccess.getColumnAccess().getEqualsSignKeyword_1());
            		
            // InternalSsql.g:410:3: ( (lv_value_2_0= RULE_STRING ) )
            // InternalSsql.g:411:4: (lv_value_2_0= RULE_STRING )
            {
            // InternalSsql.g:411:4: (lv_value_2_0= RULE_STRING )
            // InternalSsql.g:412:5: lv_value_2_0= RULE_STRING
            {
            lv_value_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_value_2_0, grammarAccess.getColumnAccess().getValueSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getColumnRule());
            					}
            					setWithLastConsumed(
            						current,
            						"value",
            						lv_value_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleColumn"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000004802L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000012010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000002010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000010010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000020L});

}