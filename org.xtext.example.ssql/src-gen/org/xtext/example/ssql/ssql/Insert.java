/**
 * generated by Xtext 2.25.0
 */
package org.xtext.example.ssql.ssql;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Insert</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.ssql.ssql.Insert#getTable <em>Table</em>}</li>
 *   <li>{@link org.xtext.example.ssql.ssql.Insert#getRows <em>Rows</em>}</li>
 * </ul>
 *
 * @see org.xtext.example.ssql.ssql.SsqlPackage#getInsert()
 * @model
 * @generated
 */
public interface Insert extends Fields
{
  /**
   * Returns the value of the '<em><b>Table</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Table</em>' attribute.
   * @see #setTable(String)
   * @see org.xtext.example.ssql.ssql.SsqlPackage#getInsert_Table()
   * @model
   * @generated
   */
  String getTable();

  /**
   * Sets the value of the '{@link org.xtext.example.ssql.ssql.Insert#getTable <em>Table</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Table</em>' attribute.
   * @see #getTable()
   * @generated
   */
  void setTable(String value);

  /**
   * Returns the value of the '<em><b>Rows</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.ssql.ssql.Row}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Rows</em>' containment reference list.
   * @see org.xtext.example.ssql.ssql.SsqlPackage#getInsert_Rows()
   * @model containment="true"
   * @generated
   */
  EList<Row> getRows();

} // Insert
