/**
 * generated by Xtext 2.25.0
 */
package org.xtext.example.ssql.ssql;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tables</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.ssql.ssql.Tables#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see org.xtext.example.ssql.ssql.SsqlPackage#getTables()
 * @model
 * @generated
 */
public interface Tables extends EObject
{
  /**
   * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.ssql.ssql.Fields}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elements</em>' containment reference list.
   * @see org.xtext.example.ssql.ssql.SsqlPackage#getTables_Elements()
   * @model containment="true"
   * @generated
   */
  EList<Fields> getElements();

} // Tables
