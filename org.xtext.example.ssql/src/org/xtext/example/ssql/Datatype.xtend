package org.xtext.example.ssql

class Datatype {
	
	static String[] userDataTypes = #["Text", "Number", "Date"];
	static String[] sqlDataTypes = #["VARCHAR", "INT", "DATE"];
	
	def static String toSQLType(String userDataType){
		
		for (i : 0 ..< userDataTypes.length) {
			if(userDataTypes.get(i).equalsIgnoreCase(userDataType)){
				return sqlDataTypes.get(i);		
			}
		}
		
		return null;
	}
}