package org.xtext.example.ssql

class Database {
	
	def static String generateId(String tableName){
		
		val id = "id_" + tableName.toLowerCase();
		
		return id;
	}
	
}