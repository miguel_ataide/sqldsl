/*
 * generated by Xtext 2.25.0
 */
package org.xtext.example.ssql.validation;

import org.eclipse.xtext.validation.Check;
import org.xtext.example.ssql.ssql.Feature;
import org.xtext.example.ssql.ssql.SsqlPackage;

/**
 * This class contains custom validation rules. 
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
public class SsqlValidator extends AbstractSsqlValidator {
	
	public static final String INVALID_TYPE = "invalidType";
	

	@Check
	public void checkGreetingStartsWithCapital(Feature feature) {
		
		if(!feature.getType().equalsIgnoreCase("String") &&
			!feature.getType().equalsIgnoreCase("INT") &&
			!feature.getType().equalsIgnoreCase("DATE")) {
			warning("Not available type",
					SsqlPackage.Literals.FEATURE__TYPE,
					INVALID_TYPE);
		}
	}
	
}
