package org.xtext.example.ssql;

import org.eclipse.xtext.xbase.lib.ExclusiveRange;

@SuppressWarnings("all")
public class Datatype {
  private static String[] userDataTypes = { "Text", "Number", "Date" };
  
  private static String[] sqlDataTypes = { "VARCHAR", "INT", "DATE" };
  
  public static String toSQLType(final String userDataType) {
    int _length = Datatype.userDataTypes.length;
    ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, _length, true);
    for (final Integer i : _doubleDotLessThan) {
      boolean _equalsIgnoreCase = (Datatype.userDataTypes[(i).intValue()]).equalsIgnoreCase(userDataType);
      if (_equalsIgnoreCase) {
        return Datatype.sqlDataTypes[(i).intValue()];
      }
    }
    return null;
  }
}
