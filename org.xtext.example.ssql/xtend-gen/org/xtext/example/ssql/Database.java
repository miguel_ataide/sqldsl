package org.xtext.example.ssql;

@SuppressWarnings("all")
public class Database {
  public static String generateId(final String tableName) {
    String _lowerCase = tableName.toLowerCase();
    final String id = ("id_" + _lowerCase);
    return id;
  }
}
